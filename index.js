/** @format */

import {AppRegistry} from 'react-native';
import { AppWrap } from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => AppWrap);
