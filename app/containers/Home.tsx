import * as React from 'react';
import { View, Text, TouchableHighlight, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { ActionCreators } from '../actions';
class Home extends React.Component {

    constructor(props) {
        super(props);
    }

    fetchDirectories = () => {
        this.props.fetchDirectories();
    }

    _getStore = async () => {
        this.props.retrieveStoredDocs();
    }

    _keyExtractor = (item, index) => { return item.id; }

    _renderItem = ({item}) => (
        <TouchableOpacity onPress={() => console.log('Pressed...')} key={item.id}>
            <View>
                <Text>{item.id}</Text><Text>{item.name}</Text>
            </View>
        </TouchableOpacity>
    );
    
    render() {

        // console.log(this.props);
        return(
            <View style={{flex: 1}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: 20}}>
                    <TouchableHighlight onPress={() => this.fetchDirectories()} >
                        <Text style={{justifyContent: 'space-around'}}>Fetch Directories Now</Text>
                    </TouchableHighlight>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: 20}}>
                    <TouchableHighlight onPress={() => this._getStore()} >
                        <Text style={{justifyContent: 'space-around'}}>Get Storage</Text>
                    </TouchableHighlight>
                </View>
                {
                    this.props.documents && this.props.documents.length > 0 && 
                    <View>
                        <FlatList
                            data={this.props.documents}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderItem}
                        />
                    </View>
                }
                {
                    this.props.error && this.props.error !== undefined && 
                    <View>
                        <Text>{this.props.error}</Text>
                    </View>
                }
            </View>
        );

    }

}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
    // console.log('State: ', state);
    return {
        directories: state.directoryReducer.directories,
        documents: state.directoryReducer.documents,
        savedDocuments: state.directoryReducer.savedDocuments,
        error: state.directoryReducer.error
    }
}

export const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(Home);