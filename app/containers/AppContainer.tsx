import * as React from 'react';
import { connect } from 'react-redux';
import { ActionCreators } from '../actions/index';
import { bindActionCreators, Dispatch } from 'redux';
import { HomeContainer }  from '../containers/Home';
import { Auth, Logger } from 'aws-amplify';
import { View, Text } from 'react-native';

const logger = new Logger('Auth'); 

class AppContainerClass extends React.Component {

    public state = {
        authed: false
    }

    public async componentDidMount() {
        await this.handleSignIn();
    }

    public handleSignIn = async () => {
        const user =  await this.signIn('christopher@swarmdigital.co.za', 'Chris123');
        if( user !== null ) {
            this.setState({
                authed: true
            });
        }
        else {
            console.log(this.state.authed);
            console.log( 'Auth: ' + JSON.stringify(user) );
        }
    } 

    public signIn = async (email: string, password: string): Promise<any | null> => {
        try {
            const user = await Auth.signIn(email, password);
            console.log(user);
            return user;
        } catch (Ex) {
            logger.error(Ex);
            return null;
        }
    };
    
    public render(): JSX.Element {
        if( this.state.authed ) {
            return ( 
                <HomeContainer {...this.props} />
            );
        }
        else {
            return (
                <View>
                    <Text>Loading...</Text>
                </View>
            );
        }

    }
}

export const AppContainer = AppContainerClass;

