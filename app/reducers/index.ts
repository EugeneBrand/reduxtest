
import { combineReducers } from 'redux';
import * as directoryReducer from './directoryReducer';

// rootReducer
const rootReducer = combineReducers(Object.assign(
    directoryReducer
));

export default rootReducer;