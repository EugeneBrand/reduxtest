import createReducer from '../lib/createReducer';
import { DocumentActionTypes, Directory, Documents, DirectoryState } from '../actions/document_types';

const INITIAL_STATE = {
    documents: [],
    directories: [],
    savedDocuments: [],
    error: undefined
}
//
export const directoryReducer = createReducer(state=INITIAL_STATE, {
    // ADD RETRIEVED DIRECTORIES && DOCUMENTS TO STATE
    [DocumentActionTypes.SET_DIRECTORIES](state={}, action) {

        let documents: Documents[] = [];
        const directories = action.data.map((dir: Directory) => {
            documents = [...documents, ...dir.documents];
            return dir;
        });

        return { 
            ...state,
            documents: documents,
            directories: directories
        };

    },
    // STORE RETRIEVED DOCUMENTS
    [DocumentActionTypes.SET_DOCUMENTS](state={}, action) {

        return {
            ...state,
            savedDocuments: action.data
        }
        
    },
    // FETCH STORED DOCUMENTS
    [DocumentActionTypes.RETRIEVE_DOCUMENTS](state={}, action) {

        return {
            ...state,
            savedDocuments: action.data
        }

    },
    // ERROR
    [DocumentActionTypes.ERROR](state={}, action) {
        
        return {
            ...state,
            error: action.data
        }

    }
});

