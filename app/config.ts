// DEV
export const config = {
	cognito: {
		REGION: 'eu-west-1',
		USER_POOL_ID: 'eu-west-1_o0Aq3HpOB',
		APP_CLIENT_ID: '4hbat8e64fqq4ce2duk9623iuu',
		IDENTITY_POOL_ID: 'eu-west-1:371d0146-a14c-4624-a447-517f6d3186fe'
	},
	api: {	
		NAME: 'dev-paperlessconstruction',
		ENDPOINT: 'https://anbekbt4dh.execute-api.eu-west-1.amazonaws.com/dev'
	},
	s3: {
		BUCKET: 'dev-assets.paperlessconstruction.co.uk',
		REGION: 'eu-west-1',
		LEVEL: 'public'
	}
};
// UAT
// export const config = {
// 	cognito: {
// 		REGION: 'eu-west-1',
// 		USER_POOL_ID: 'eu-west-1_lHuHNUR6r',
// 		APP_CLIENT_ID: '3tpe3ofuchb6am5dmg0v48fbb0',
// 		IDENTITY_POOL_ID: 'eu-west-1:c5b203f8-c71a-490d-aa6f-60ff91a3392e'
// 	},
// 	api: {	
// 		NAME: 'uat-paperlessconstruction',
// 		ENDPOINT: 'https://1h4rm3d82c.execute-api.eu-west-1.amazonaws.com/uat'
// 	},
// 	s3: {
// 		BUCKET: 'uat-assets.paperlessconstruction.co.uk', 
// 		REGION: 'eu-west-1',
// 		LEVEL: 'public'
// 	}
// }