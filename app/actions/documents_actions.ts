import { DocumentActionTypes, Directory, Documents, DirectoryState } from '../actions/document_types';
import { action } from 'typesafe-actions';
import { Storage } from 'aws-amplify';
import { getStoredState } from 'redux-persist';
import RNFetchBlob from 'rn-fetch-blob';

// Fetch directory list and dispatch setDirectories to save to state
export const fetchDirectories = () => {
    return async ( dispatch, getState ) => {
        const response = await fetch('https://anbekbt4dh.execute-api.eu-west-1.amazonaws.com/dev/documents/directories/?directory=all&client_id=1');
        const data = await response.json();
        if( data.message !== 'Documents not found' ) {
            dispatch( setDirectories({ directories: data }) );
            // Pass in document array
            dispatch( downloadDocuments(getState().directoryReducer.documents));
        }
        else {
            dispatch( dispatchError(data.message) );
        }
    }
}
// Store directory, document arrays into state
export const setDirectories = ({ directories }) => {
    return {
        type: DocumentActionTypes.SET_DIRECTORIES,
        data: directories
    };
}
// Download doc list asynchronously
export const downloadDocuments = ( data: Documents[] ) => async( dispatch ) => {

    let dirs = RNFetchBlob.fs.dirs; // File path
    // console.log( data ); 
    const savedFiles: any = await Promise.all( data.map( async(doc) => {

        const key = doc.s3_key.substr(doc.s3_key.lastIndexOf('/'), doc.s3_key.length ); // Storage key
        // console.log(key);

        const filename = doc.id; // Ref Id

        const url: any = await Storage.get(doc.s3_key, { level: 'public'}); // Aws storage fetch url

        const response = await RNFetchBlob.config({ path: dirs.DocumentDir + key }).fetch('GET', url); // RNFetchBlob

        // console.log( response.path() );

        return { [filename]: response.path() } ;
        
    })).catch( onrejected => {
        console.log('Download documents failed...');
        dispatch( dispatchError(onrejected) );
    });
    if(savedFiles.message !== undefined ) {
        dispatch( setDocument(savedFiles) );
    }
    else {
        dispatch( dispatchError(savedFiles.message) );
    }
};
// Action to save doc to storage
export const setDocument = ( savedDocuments: [] ) => {
    return {
        type: DocumentActionTypes.SET_DOCUMENTS,
        data: savedDocuments
    }
}
//
export const retrieveStoredDocs = () => {
    return async (dispatch, getState ) => {
        const retrievedFiles = await getState().directoryReducer.savedDocuments;
        dispatch({
            type: DocumentActionTypes.RETRIEVE_DOCUMENTS,
            data: retrievedFiles
        });
    }
}
// ERROR
export const dispatchError = ( message: string ) => {
    return {
        type: DocumentActionTypes.ERROR,
        data: message
    }
}
