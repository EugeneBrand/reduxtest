export enum DocumentActionTypes {
    GET_DIRECTORIES = '@@documents/GET_DIRECTORIES',
    SET_DIRECTORIES = '@@documents/SET_DIRECTORIES',
	GET_DOCUMENTS = '@@documents/GET_DOCUMENTS',
	SET_DOCUMENTS = '@@documents/SET_DOCUMENTS',
	RETRIEVE_DOCUMENTS = '@@document/RETRIEVE_DOCUMENTS',
	ERROR = '@@documents/ERROR'
}
export interface Directory {
	id: string;
	path: string;
	path_nodes: string;
	documents: Documents[];
	type: 'shared' | 'templates' | 'trash';
}
export interface Documents {
	id: string;
	ref_id: string;
	client_id: string;
	contractor_id: string;
	type_id: string;	
	archived: boolean;
	s3_key: string;
	reference: string;
	version: number;
	name: string;
	date: Date;	
	access_level?: number;
}
export interface DirectoryState {
    readonly loading: boolean,
    readonly directories: Directory[],
	readonly documents: Documents[],
	readonly savedDocuments: any,
	readonly errors?: string
}