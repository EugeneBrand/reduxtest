import * as DocumentActions from './documents_actions';

export const ActionCreators = {
    ...DocumentActions
};

// export type IActionCreators = DocumentActions.IDocumentActions;