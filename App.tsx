import * as React from 'react';
import { Text } from 'react-native';
import { AppContainer } from './app/containers/AppContainer';
// REDUX & MIDDLEWARE
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import logger, { createLogger } from 'redux-logger';
// ROOT REDUCER
import rootReducer from './app/reducers';
import { config } from './app/config'
// AWS Amplify
import Auth from '@aws-amplify/auth';
import API from '@aws-amplify/api';
import Storage from '@aws-amplify/storage';
// REDUX PERSIST
import { persistStore, persistReducer, persistCombineReducers, PersistedState } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import FilesystemStorage from 'redux-persist-filesystem-storage';
import RNFetchBlob from 'rn-fetch-blob';

// REDUX-LOGGER
const loggerMiddleware = createLogger({ predicate: () => __DEV__ });

// Local storage config
FilesystemStorage.config({
	storagePath: `${RNFetchBlob.fs.dirs.DocumentDir}/persistStore`
});

// persistConfig
const persistConfig = {
	key: 'root',
	storage: FilesystemStorage
}
// Load state
const loadState = async (): Promise<string | undefined> => {
	try {
		const seriealizedState = await FilesystemStorage.getItem('state', (error, result) => {
			if (!result) {
				return undefined;
			}
			else {
				return JSON.parse(result);
			}
		});
		console.log(seriealizedState);
		return seriealizedState;
	}
	catch (e) {
		console.log(e);
		return undefined;
	}
}
// Save state
const saveState = (state) => {
	try {
		const seriealizedState = JSON.stringify(state);
		console.log(seriealizedState);
		FilesystemStorage.setItem('state', seriealizedState );
	}
	catch (e) {
		console.log(e);
		return undefined;
	}
}
// Persist reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);

// Load state from file storage
export const persistedState = async() => await loadState();

// Store config
function configureStore( persistedState ) {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  );
  return createStore( persistedReducer, persistedState, enhancer );
}
// Create store with config
const store = configureStore({});

// Subscribe to changes in state and save state
store.subscribe(() => saveState(store.getState()));

// Persistor
const persistor = persistStore(store);

// App
export const AppWrap = () => (
  <Provider store={store}>
		<PersistGate loading={<Text>Loading...</Text>} persistor={persistor}>
    		<AppContainer />
		</PersistGate>
  </Provider>
);

// AWS Authentication
Auth.configure({
	mandatorySignIn: true,
	region: config.cognito.REGION,
	userPoolId: config.cognito.USER_POOL_ID,
	identityPoolId: config.cognito.IDENTITY_POOL_ID,
	userPoolWebClientId: config.cognito.APP_CLIENT_ID
});
// AWS API
API.configure({
	endpoints: [
		{
			name: config.api.NAME,
			endpoint: config.api.ENDPOINT
		}
	]
});
// AWS Storage / S3
Storage.configure({
	bucket: config.s3.BUCKET,
	region: config.s3.REGION,
	identityPoolId: config.cognito.IDENTITY_POOL_ID,
	level: config.s3.LEVEL
});


